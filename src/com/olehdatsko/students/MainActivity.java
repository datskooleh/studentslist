package com.olehdatsko.students;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

public class MainActivity extends Activity {

	public static final String EXTRA_FIRST_NAME = "first_name";
	public static final String EXTRA_LAST_NAME = "last_name";
	public static final String EXTRA_DOB = "dob";

	//private AbsListView list;
	private StudentAdapter adapter;

	private Spinner spinner;

	private ArrayList<Student> students;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if(savedInstanceState == null)
			students = Generator.generate();
		else
			students = savedInstanceState.getParcelableArrayList("studentsList");
		
		adapter = new StudentAdapter(students, this);

		OnItemClickListener listener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent intent = new Intent(getApplicationContext(),
						DetailActivity.class);

				Student student = (Student) adapter.getItem(position);

				intent.putExtra(EXTRA_FIRST_NAME, student.firstName);
				intent.putExtra(EXTRA_LAST_NAME, student.lastName);
				intent.putExtra(EXTRA_DOB, student.dob);

				startActivity(intent);

				//startActivityForResult(intent, 111);
			}
		};

		//list = (AbsListView) findViewById(R.id.list);

		View list = findViewById(R.id.list);
		
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {

			if (list instanceof ListView){
				ListView l1 = (ListView) list; 
				l1.setAdapter(adapter);
				l1.setOnItemClickListener(listener);
			}else if (list instanceof GridView){

				GridView g1 = (GridView) list;
				g1.setAdapter(adapter);
				g1.setOnItemClickListener(listener);
			}

		} else {

			AbsListView al1 = (AbsListView)list;

			al1.setAdapter(adapter);
			al1.setOnItemClickListener(listener);
		}

		//list.setOnItemClickListener();

		String[] titles = getResources().getStringArray(R.array.titles);

		spinner = (Spinner) findViewById(R.id.sort);

		SpinnerAdapter spinnerAdapter = new ArrayAdapter<String>(
				getApplicationContext(), android.R.layout.simple_list_item_1,
				titles);

		spinner.setAdapter(spinnerAdapter);
		
		if(savedInstanceState != null)
			spinner.setSelection(savedInstanceState.getInt("spinnerItem"));

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(getApplicationContext(),
						"Sort by " + ((position == 0) ? "name" : "age"),
						Toast.LENGTH_LONG).show();
				if (position == 0)
					Collections.sort(students, new NamesComparator());
				else
					Collections.sort(students, new AgesComparator());

				adapter.notifyDataSetChanged();

				adapter.notifyDataSetInvalidated();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putParcelableArrayList("studentsList", students);
		outState.putInt("spinnerItem", spinner.getSelectedItemPosition());
		
		super.onSaveInstanceState(outState);
	}
}

/*
 * compare items using it's first and last names (full comparing)
 */
class NamesComparator implements Comparator<Student>{
	public int compare(Student a, Student b){
		return a.firstName.concat(a.lastName).compareTo(b.firstName.concat(b.lastName));
	}
}

/*
 * compare items using it's ages
 */
class AgesComparator implements Comparator<Student>{
	public int compare(Student a, Student b){
		if(a.dob > b.dob)
			return 1;
		else if(a.dob < b.dob)
			return -1;
		else
			return 0;
	}
}